app.controller('weatherController', ['$scope','$http', function($scope,$http){

	$scope.weatherIcon = '';


	// We currently don't care about day/night cycle
  // https://openweathermap.org/weather-conditions
	$scope.iconsMap = new Map();
  
  // Too much info, to ensure we can switch to a more detailed icon set later on
  // TODO: Find a better way to store this info
  $scope.iconsMap.set(200,{'day':'wi-day-thunderstorm','night':'wi-night-thunderstorm'});
  $scope.iconsMap.set(201,{'day':'wi-day-thunderstorm','night':'wi-night-thunderstorm'});
  $scope.iconsMap.set(202,{'day':'wi-day-thunderstorm','night':'wi-night-thunderstorm'});
  $scope.iconsMap.set(210,{'day':'wi-day-thunderstorm','night':'wi-night-thunderstorm'});
  $scope.iconsMap.set(211,{'day':'wi-day-thunderstorm','night':'wi-night-thunderstorm'});
  $scope.iconsMap.set(212,{'day':'wi-day-thunderstorm','night':'wi-night-thunderstorm'});
  $scope.iconsMap.set(221,{'day':'wi-day-thunderstorm','night':'wi-night-thunderstorm'});
  $scope.iconsMap.set(230,{'day':'wi-day-thunderstorm','night':'wi-night-thunderstorm'});
  $scope.iconsMap.set(231,{'day':'wi-day-thunderstorm','night':'wi-night-thunderstorm'});
  $scope.iconsMap.set(232,{'day':'wi-day-thunderstorm','night':'wi-night-thunderstorm'});
  
  // Group 3XX - Drizzle
  $scope.iconsMap.set(300,{'day':'wi-day-showers','night':'wi-night-showers'});
  $scope.iconsMap.set(301,{'day':'wi-day-showers','night':'wi-night-showers'});
  $scope.iconsMap.set(302,{'day':'wi-day-showers','night':'wi-night-showers'});
  $scope.iconsMap.set(310,{'day':'wi-day-showers','night':'wi-night-showers'});
  $scope.iconsMap.set(311,{'day':'wi-day-showers','night':'wi-night-showers'});
  $scope.iconsMap.set(312,{'day':'wi-day-showers','night':'wi-night-showers'});
  $scope.iconsMap.set(313,{'day':'wi-day-showers','night':'wi-night-showers'});
  $scope.iconsMap.set(313,{'day':'wi-day-showers','night':'wi-night-showers'});
  $scope.iconsMap.set(321,{'day':'wi-day-showers','night':'wi-night-showers'});
  
  // Group 5XX - Rain
  $scope.iconsMap.set(500,{'day':'wi-day-rain','night':'wi-night-rain'});
  $scope.iconsMap.set(501,{'day':'wi-day-rain','night':'wi-night-rain'});
  $scope.iconsMap.set(502,{'day':'wi-day-rain','night':'wi-night-rain'});
  $scope.iconsMap.set(503,{'day':'wi-day-rain','night':'wi-night-rain'});
  $scope.iconsMap.set(504,{'day':'wi-day-rain','night':'wi-night-rain'});
  $scope.iconsMap.set(511,{'day':'wi-day-rain','night':'wi-night-rain'});
  $scope.iconsMap.set(520,{'day':'wi-day-rain','night':'wi-night-rain'});
  $scope.iconsMap.set(521,{'day':'wi-day-rain','night':'wi-night-rain'});
  $scope.iconsMap.set(522,{'day':'wi-day-rain','night':'wi-night-rain'});
  $scope.iconsMap.set(531,{'day':'wi-day-rain','night':'wi-night-rain'});
  
  // Group 6XX - Snow
  $scope.iconsMap.set(600,{'day':'wi-day-snow','night':'wi-night-snow'});
  $scope.iconsMap.set(601,{'day':'wi-day-snow','night':'wi-night-snow'});
  $scope.iconsMap.set(602,{'day':'wi-day-snow','night':'wi-night-snow'});
  $scope.iconsMap.set(611,{'day':'wi-day-snow','night':'wi-night-snow'});
  $scope.iconsMap.set(612,{'day':'wi-day-snow','night':'wi-night-snow'});
  $scope.iconsMap.set(615,{'day':'wi-day-snow','night':'wi-night-snow'});
  $scope.iconsMap.set(616,{'day':'wi-day-snow','night':'wi-night-snow'});
  $scope.iconsMap.set(620,{'day':'wi-day-snow','night':'wi-night-snow'});
  $scope.iconsMap.set(621,{'day':'wi-day-snow','night':'wi-night-snow'});
  $scope.iconsMap.set(622,{'day':'wi-day-snow','night':'wi-night-snow'});
  
  // Group 7XX - Atmosphere
  $scope.iconsMap.set(701,{'day':'wi-day-fog','night':'wi-night-fog'});
  $scope.iconsMap.set(711,{'day':'wi-smoke','night':'wi-smoke'});
  $scope.iconsMap.set(721,{'day':'wi-day-haze','night':'wi-day-haze'});
  $scope.iconsMap.set(731,{'day':'wi-day-fog','night':'wi-night-fog'});
  $scope.iconsMap.set(741,{'day':'wi-day-fog','night':'wi-night-fog'});
  $scope.iconsMap.set(751,{'day':'','night':''});
  $scope.iconsMap.set(761,{'day':'','night':''});
  $scope.iconsMap.set(762,{'day':'','night':''});
  $scope.iconsMap.set(771,{'day':'','night':''});
  $scope.iconsMap.set(781,{'day':'','night':''});
  
  // Group 800 - Clear
  $scope.iconsMap.set(800,{'day':'wi-day-sunny','night':'wi-night-clear'});
  
  // Group 80X - Clouds
  $scope.iconsMap.set(801,{'day':'wi-day-cloudy','night':'wi-night-cloudy'});
  $scope.iconsMap.set(802,{'day':'wi-day-cloudy','night':'wi-night-cloudy'});
  $scope.iconsMap.set(803,{'day':'wi-day-cloudy','night':'wi-night-cloudy'});
  $scope.iconsMap.set(804,{'day':'wi-day-cloudy','night':'wi-night-cloudy'});
  
  // Group 90X - Extreme
  $scope.iconsMap.set(900,{'day':'','night':''});
  $scope.iconsMap.set(901,{'day':'','night':''});
  $scope.iconsMap.set(902,{'day':'','night':''});
  $scope.iconsMap.set(903,{'day':'','night':''});
  $scope.iconsMap.set(904,{'day':'','night':''});
  $scope.iconsMap.set(905,{'day':'','night':''});
  $scope.iconsMap.set(906,{'day':'','night':''});
  
  // Group 9XX - Additional
  $scope.iconsMap.set(951,{'day':'','night':''});
  $scope.iconsMap.set(952,{'day':'','night':''});
  $scope.iconsMap.set(953,{'day':'','night':''});
  $scope.iconsMap.set(954,{'day':'','night':''});
  $scope.iconsMap.set(955,{'day':'','night':''});
  $scope.iconsMap.set(956,{'day':'','night':''});
  $scope.iconsMap.set(957,{'day':'','night':''});
  $scope.iconsMap.set(958,{'day':'','night':''});
  $scope.iconsMap.set(959,{'day':'','night':''});
  $scope.iconsMap.set(960,{'day':'','night':''});
  $scope.iconsMap.set(961,{'day':'','night':''});
  $scope.iconsMap.set(962,{'day':'','night':''});
  
  // Functions
	$scope.getWeather = function(){
  	console.log("Getting weather");
    $http.get('http://api.openweathermap.org/data/2.5/weather', {params:$scope.getParams})
      .then($scope.positiveCallback, $scope.negativeCallback);
  }

	$scope.setTimer = function(){
  	setTimeout(function(){
    	$scope.getWeather();
    }, 60000)
  }

  $scope.positiveCallback = function(data){
		data.data.weather = data.data.weather[0];// Moving data out of an array with only one entry
    $scope.weather = data.data;
    $scope.setWeathericon();
    $scope.setTimer();
  }

  $scope.negativeCallback = function(data){
    console.log("Failed to get weather... What to do?!");
		$scope.setTimer();
  }

  $scope.getParams = {
      q:'kolding,dk',
      units:'metric',
      appid:'778155ff9ca2ab64b347df20c46ff771'
  }
  
	$scope.setWeathericon = function(){
		var weatherIcon = $scope.iconsMap.get($scope.weather.weather.id);
		var currentTime = $scope.getCurrentTimeInUnix();
    if(currentTime > $scope.weather.sys.sunset){
    	$scope.weatherIcon = weatherIcon.night;
    }else{
    	$scope.weatherIcon = weatherIcon.day;
    }
	
  }
  
  $scope.getCurrentTimeInUnix = function(){
  	return Date.now()/1000;
  }
  
  // Init code
  $scope.weather = $scope.getWeather();

}]);